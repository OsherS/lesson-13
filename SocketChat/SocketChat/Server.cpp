#include "Server.h"


Server::Server()
{

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET,  SOCK_STREAM,  IPPROTO_TCP); 

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{
	
	struct sockaddr_in sa = { 0 };
	
	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	
	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}


void Server::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	// the function that handle the conversation with the client
	std::thread t1(&Server::clientHandler, this, client_socket); // spawn new thread that calls bar(0)
	t1.detach();
}

//handles clients server request
void Server::clientHandler(SOCKET clientSocket)
{
	std::string username = "";
	try
	{
		std::string content = "";
		bool once = true;
		int bytes = 0;
		int userlen = 0; //2 bytes
		int msglen = 0; //5b
		string tempname = "";
		string secuser = "";
		std::string msg = "";

		bytes = Helper::getMessageTypeCode(clientSocket);

		if (bytes == 200)
		{
			userlen = Helper::getIntPartFromSocket(clientSocket, 2);
			username = Helper::getStringPartFromSocket(clientSocket, userlen);
			vec.push_back(username);
			Helper::send_update_message_to_client(clientSocket, "", "", vec_to_str(this->vec)); //update later
			while (true)
			{
				bytes = Helper::getMessageTypeCode(clientSocket);
				if (bytes == 204)
				{

					userlen = Helper::getIntPartFromSocket(clientSocket, 2);
					secuser = Helper::getStringPartFromSocket(clientSocket, userlen);
					msglen = Helper::getIntPartFromSocket(clientSocket, 5);
					msg = Helper::getStringPartFromSocket(clientSocket, msglen);

					tempname = firstSecondName(username, secuser, 1) + "&" + firstSecondName(username, secuser, 2) + ".txt";
					std::ifstream ifs(tempname);
					string content((std::istreambuf_iterator<char>(ifs)),
						(std::istreambuf_iterator<char>()));

					Helper::send_update_message_to_client(clientSocket, content, secuser, vec_to_str(vec)); 

					if (msglen > 0)
					{
						
						ofstream MyFile(tempname, ios::app);
						content = "&MAGSH_MESSAGE&&Author&" + username + "&DATA&" + msg;
						MyFile << content;
						
					}
				}
				

			}
		}
	}
	catch (const std::exception& e)
	{
		vec.erase(std::remove(vec.begin(), vec.end(), username), vec.end()); //removes offline users
		closesocket(clientSocket);
	}


}

std::string Server::vec_to_str(std::vector<std::string> vec) //vector to string
{
	std::string str = "";

	for (int i = 0; i < vec.size(); i++)
	{
		str += vec[i];
		if (i != vec.size() - 1)
		{
			str += "&";
		}
	}
	
	return str;
}

//if pick = 1 returns the first name, if pick = 2 returns the second name (Alphabetical)
string Server::firstSecondName(string str1, string str2, int pick)
{
	if (str1 > str2)
	{
		if (pick == 1)
		{
			return str2;
		}
		return str1;
	}

	if (str1 < str2)
	{
		if (pick == 1)
		{
			return str1;
		}
		return str2;
	}

	return str1;
}

