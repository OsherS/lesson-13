#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <exception>
#include <iostream>
#include <string>
#include "Helper.h"
#include <algorithm>
#include <vector>
#include <thread>
#include <fstream>
using namespace std;


class Server
{
public:
	Server();
	~Server();
	void serve(int port);
	std::string vec_to_str(std::vector<std::string> vec);
	string firstSecondName(string str1, string str2, int pick); 


private:

	void accept();
	void clientHandler(SOCKET clientSocket);
	std::vector<std::string> vec;
	SOCKET _serverSocket;
};

